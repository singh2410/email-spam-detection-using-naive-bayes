#!/usr/bin/env python
# coding: utf-8

# ## Email spam detection using NLTK & Naive Bayes
# #By- Aarush Kumar
# #Dated: May 19,2021

# In[2]:


get_ipython().system('pip install nltk')
get_ipython().system('pip install autocorrect')
get_ipython().system('pip install scikit-learn')


# In[3]:


from nltk.tokenize import word_tokenize 
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
stemmer=PorterStemmer()
from wordcloud import WordCloud
import pandas as pd
import matplotlib.pyplot as plt
from autocorrect import Speller
spell=Speller()
from tqdm import tqdm
import re


# In[4]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Email spam/spam.csv')


# In[5]:


df


# In[6]:


df.drop(['Unnamed: 2','Unnamed: 3','Unnamed: 4'],axis=1,inplace=True)


# In[7]:


df.head()


# In[8]:


df.rename(columns={'v1':'labels','v2':'messages'},inplace=True)


# In[9]:


df.head()


# In[10]:


#visualizing spam words
spam_words= ' '.join(list(df[df['labels']=='spam']['messages']))
spam_wc= WordCloud(width=500,height=500).generate(spam_words)
plt.figure(figsize=(10,8))
plt.imshow(spam_wc)
plt.show()


# In[11]:


#visualizing ham words
spam_words= ' '.join(list(df[df['labels']=='ham']['messages']))
spam_wc= WordCloud(width=500,height=500).generate(spam_words)
plt.figure(figsize=(10,8))
plt.imshow(spam_wc)
plt.show()


# In[13]:


df.head()


# In[14]:


df.shape


# In[15]:


# Encoding our Labels
df['labels']=df['labels'].apply(lambda x: 1 if x=='spam' else 0)


# In[17]:


# preprocessing the Messages
df1=[]
import nltk
nltk.download('punkt')
for i in tqdm(range(df.shape[0])):
    lines=df.iloc[i,1]
    # removing non alphabatic characters
    lines= re.sub('[^A-Za-z]',' ',lines)
    # lowering the every word
    lines=lines.lower()
    
    # tokenization
    tokenized_lines=word_tokenize(lines)
    
    # removing stop words ,stemming and spell correction
    processed_lines=[]
    for i in tokenized_lines:
        if i not in set(stopwords.words('english')):
            processed_lines.append(spell(stemmer.stem(i)))
            
    final_lines=' '.join(processed_lines)
    df1.append(final_lines)


# In[18]:


df1


# In[22]:


Y=df['labels']


# In[24]:


Y.value_counts()


# In[25]:


#splitting data
from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test=train_test_split(df1,Y,test_size=0.25)


# In[26]:


X_train


# In[27]:


X_test


# In[28]:


Y_train


# In[29]:


Y_test


# In[30]:


#Vectorizing using BOW
#tf_idf
from sklearn.feature_extraction.text import CountVectorizer  
matrix=CountVectorizer()


# In[31]:


matrix


# In[32]:


X_train_vect=matrix.fit_transform(X_train).toarray()
X_test_vect=matrix.transform(X_test).toarray()


# In[33]:


X_train_vect


# In[34]:


X_test_vect


# In[35]:


Y_train


# In[36]:


Y_test


# In[37]:


#Training model(Naive Bayes)
from sklearn.naive_bayes import GaussianNB
model=GaussianNB()


# In[38]:


model.fit(X_train_vect,Y_train)


# In[45]:


model.class_count_


# In[40]:


Y_pred=model.predict(X_test_vect)


# In[41]:


Y_pred


# In[42]:


from sklearn.metrics import accuracy_score, confusion_matrix


# In[43]:


accuracy_score(Y_test,Y_pred) * 100


# In[44]:


confusion_matrix(Y_test,Y_pred)

