# Email spam detection using Naive Bayes
# By- Aarush Kumar
E-mail spam detection using NLP and Naive Bayes.
In this project I have used NLTK library for NLP processing and used Bag of Words.
At last in order to train my model I used Naive Bayes algorithm.
Steps followed for NLP are as follows-

for Preprocessing NLP Data
  1.removing non alphabatic characters
  2.lowering the format
  3.tokeninzation
  4.remove stop words
  5.steming / lemitization
  6.spell correction

  Thankyou!

